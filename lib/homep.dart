import 'package:flutter/material.dart';
import 'package:flutter_module4/edit_profile.dart';
//import 'package:loginapp/main.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.greenAccent,
        appBar: AppBar(
          title: const Text("Dashboard"),
          centerTitle: true,
        ),
        floatingActionButton: buildNavigateButton(),
        body: OrientationBuilder(builder: (context, orientation) {
          return GridView.count(
            crossAxisCount: orientation == Orientation.portrait ? 2 : 2,
            crossAxisSpacing: 5,
            children: List.generate(4, (index) {
              return Image.asset(
                'assets/nike$index.png',
                width: 200.0,
                height: 200.0,
                //fit: BoxFit.cover,
              );
            }),
          );
        }),
      );
  Widget buildNavigateButton() => FloatingActionButton.extended(
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white,
        icon: const Icon(Icons.edit),
        label: const Text('Edit Profile'),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) {
              return const Editprofile();
            }),
          );
        },
      );
}
