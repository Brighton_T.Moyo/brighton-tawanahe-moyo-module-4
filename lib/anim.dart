import 'package:flutter/material.dart';
import 'package:flutter_module4/login.dart';
import 'package:shimmer/shimmer.dart';

import 'dart:async';

class AnimSplash extends StatefulWidget {
  const AnimSplash({Key? key}) : super(key: key);

  @override
  State<AnimSplash> createState() => _AnimSplashState();
}

class _AnimSplashState extends State<AnimSplash> {
  @override
  void initState() {
    super.initState();

    Checkpage().then((status) {
      if (status) {
        navtologinpage();
      }
    });
  }

  // ignore: non_constant_identifier_names
  Future<bool> Checkpage() async {
    await Future.delayed(const Duration(milliseconds: 8000), () {});
    return true;
  }

  void navtologinpage() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => const loginpage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      // ignore: avoid_unnecessary_containers
      body: Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Opacity(opacity: 0.5, child: Image.asset('assets/an.png')),
            Shimmer.fromColors(
              baseColor: const Color(0xff01579b),
              highlightColor: const Color(0xffe100ff),
              // ignore: avoid_unnecessary_containers
              child: Container(
                child: Text(
                  "Helo",
                  style: TextStyle(
                      fontSize: 100.0,
                      fontFamily: 'Pacifico',
                      shadows: <Shadow>[
                        Shadow(
                            blurRadius: 19.0,
                            color: Colors.black26,
                            offset: Offset.fromDirection(110, 12))
                      ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
